﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class virtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {

	private Image bgImg;
	private Image joystickImg;
	public Vector3 InputDirection { set; get; }
	public Boundary myBoundary;
	public bool pressed = false;

	private void Start()
	{
 
		bgImg = GetComponent<Image>();
		joystickImg = transform.GetChild(0).GetComponent<Image>();
		InputDirection = Vector3.zero;
	}


	public virtual void OnDrag(PointerEventData ped)
	{
		Vector2 pos = Vector2.zero;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle
			(bgImg.rectTransform,
				ped.position,
				ped.pressEventCamera,
				out pos))
		{
			pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

//			float x = (bgImg.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
//			float y = (bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

			float x = Mathf.Clamp ((bgImg.rectTransform.pivot.x == .3) ? pos.x * 2 + 1 : pos.x * 2 - 1, myBoundary.xMin, myBoundary.xMax);
			float y = Mathf.Clamp ((bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1, myBoundary.zMin, myBoundary.zMax);

	 
			InputDirection = new Vector3(x,0.0f,y);

			InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;

			joystickImg.rectTransform.anchoredPosition = 
				new Vector3(InputDirection.x * (bgImg.rectTransform.sizeDelta.x / 4),
					InputDirection.z * (bgImg.rectTransform.sizeDelta.y / 4));

		}
	}

	public virtual void OnPointerDown(PointerEventData ped)
	{
		OnDrag(ped);
		pressed = true;
	}

	public virtual void OnPointerUp(PointerEventData ped)
	{
		InputDirection = Vector3.zero;
		joystickImg.rectTransform.anchoredPosition = Vector3.zero;
		pressed = false;
	}



}
