﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	
	public Rigidbody myRigidBody;
	public Transform myTransform;
	public float speed;


	// Use this for initialization
	void Start () {
		myTransform = GetComponent<Transform>();
		myRigidBody = GetComponent<Rigidbody>();
		myRigidBody.velocity = myTransform.forward * speed;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
