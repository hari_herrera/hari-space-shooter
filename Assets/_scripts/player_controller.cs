﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class player_controller : MonoBehaviour 
{
	
	public float Speed;
	public float tilt;
	public Boundary myBoundary;

	public GameObject missilePrefab;
	public float fireRate;
	public float nextFire;
	public Transform fireCannon;
	public AudioSource myAudio;

	public virtualJoystick moveJoystick;

	private Rigidbody rigidBody_Go ;
	private Quaternion calibrationQuaternion;
	void Start() {
		rigidBody_Go = GetComponent<Rigidbody>();
		myAudio = GetComponent<AudioSource>();
		CalibrateAcceleromeTer ();

  	}

	void Update() 
	{

		if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			Instantiate(missilePrefab, fireCannon.position, fireCannon.rotation);
			myAudio.Play ();
		}

		/*JOYSTICK MOVEMENT*/
		if(moveJoystick.InputDirection != Vector3.zero){ 
			rigidBody_Go.position = moveJoystick.InputDirection * 10;
		}
	}


	void FixedUpdate () 
	{
//		float moveHorizontal = Input.GetAxis ("Horizontal");
//		float moveVertical = Input.GetAxis ("Vertical");

//		Vector3 movement = new Vector3(moveHorizontal,0.0f,moveVertical);
//		Vector3 accelerationRaw = Input.acceleration;
//		Vector3 acceleration = fixAcceleration (accelerationRaw);
//		Vector3 movement = new Vector3(acceleration.x,0.0f,acceleration.y);
//
//		rigidBody_Go.velocity = movement * Speed;
//
//		rigidBody_Go.rotation = Quaternion.Euler (0.0f, 0.0f, rigidBody_Go.velocity.x * -tilt);
//	
//		rigidBody_Go.position = new Vector3 (
//			Mathf.Clamp(rigidBody_Go.position.x, myBoundary.xMin,  myBoundary.xMax),
//			0.0f,
//			Mathf.Clamp(rigidBody_Go.position.z, myBoundary.zMin,  myBoundary.zMax)
//		);
	}
	void CalibrateAcceleromeTer() {
		Vector3 accelerationSnapshot = Input.acceleration;
		Quaternion rotateQuaternion = Quaternion.FromToRotation (new Vector3 (0.0f, 0.0f, -1.0f), accelerationSnapshot);
		calibrationQuaternion = Quaternion.Inverse (rotateQuaternion);
	}
	Vector3 fixAcceleration (Vector3 acceleration){
		Vector3 fixedAcceleration = calibrationQuaternion * acceleration;
		return fixedAcceleration;
	}
}

