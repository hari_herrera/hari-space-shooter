﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameController : MonoBehaviour {

	public GameObject[] hazards;
	public Vector3 spawnValues; 
	public float hazardCount;
	public float spawnInterval;
	public float spawnStartTime;
	public float waveInterval;
	public Text scoreValue;
	public Text restartText;
	public Text gameOverText;
	private int score;

	private bool gameOver;
	public bool restart;


	// Use this for initialization
	void Start () {
		StartCoroutine	(spawnWaves ());
		scoreValue =  GameObject.Find("scoreTextValue").GetComponent<Text> ();
		restartText =  GameObject.Find("restartText").GetComponent<Text> ();
		gameOverText =  GameObject.Find("gameOverText").GetComponent<Text> ();
		score = 0;
		UpdateScore ();

	}
	
	// Update is called once per frame
	void Update(){
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R) || Input.GetButton("Jump")) {
				RestartLevel ();
			}
		}

 	}
	public void RestartLevel(){
		SceneManager.LoadScene("main", LoadSceneMode.Single);
	}
	void UpdateScore () {
		scoreValue.text= score.ToString();
	}

	public void addScore(int newscore){
		score += newscore;
		UpdateScore ();
	}

	public void gameEnds () {
		restartText.text = "Press 'R' or '2fingers Touch' to restart";
		gameOverText.text = "¡GAME OVER!";
		restart = true;
		gameOver = true;
	}

 

	IEnumerator	spawnWaves() {
		if (hazards.Length > 1) {

			while (true) {
				yield return new WaitForSeconds (spawnStartTime);
				for (int i = 0; i < hazardCount; i++) {
					GameObject hazard = hazards [Random.Range (0, hazards.Length)];
					Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x),spawnValues.y,spawnValues.z);
					Quaternion spawnRotation = Quaternion.identity;
					Instantiate(hazard, spawnPosition, spawnRotation);
					yield return new WaitForSeconds (spawnInterval);
				}
				yield return new WaitForSeconds (waveInterval);
				if (gameOver) {
					break;
				}

			}
		}


	}



}
