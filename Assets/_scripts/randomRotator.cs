﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomRotator : MonoBehaviour {

	private float tumble;
	private Rigidbody rotado;
	// Use this for initialization
	void Start () {
		tumble = 5;
		rotado = gameObject.GetComponent<Rigidbody> ();
		rotado.angularVelocity = Random.insideUnitSphere * tumble;
	}
	 
}
