﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyByContact : MonoBehaviour {
 	
	public GameObject explosion;
	public GameObject playerExplotion;
	public int scorePoint;
 	private gameController myGameController;


	void Start(){
		GameObject myGameController_Go = GameObject.FindGameObjectWithTag("GameController");
		if (myGameController_Go) {
			myGameController = myGameController_Go.GetComponent<gameController>();
		}
		if (!myGameController) {
			Debug.Log ("cannot find Game Controller");
		}
	}

	void OnTriggerEnter(Collider other) {
		if(other.tag == "boundary") {
			return;
		}
		Instantiate(explosion, transform.position, transform.rotation);
		if (other.tag == "player") {
			Instantiate(playerExplotion, other.transform.position, other.transform.rotation);
			myGameController.gameEnds ();
		}
		myGameController.addScore (scorePoint);
		Destroy(other.gameObject);
		Destroy(gameObject);
	} 
}
