﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class TouchControl : MonoBehaviour {
	public gameController gameControllerGo;
	public Scene escena;
	public Text tCount;
	public Text tHit;
	Rigidbody gObj_Rb;
	Transform gObj_T;
	GameObject gObj = null;
	Plane objPlane;
	Vector3 m0;

	Ray GenerateTouchRay(Vector3 touchPos){
		Vector3 touchPosFar = new Vector3 (touchPos.x, touchPos.y, Camera.main.farClipPlane);
		Vector3 touchPosNear = new Vector3 (touchPos.x, touchPos.y, Camera.main.nearClipPlane);
		Vector3 touchPosF = Camera.main.ScreenToWorldPoint (touchPosFar);
		Vector3 touchPosN = Camera.main.ScreenToWorldPoint (touchPosNear);
		Ray mr = new Ray (touchPosN, touchPosF - touchPosN);
		return mr;
	}





	void Update() {
		tCount.text = Input.touchCount.ToString ();
		if (Input.touchCount == 2 && gameControllerGo.restart) {
			SceneManager.LoadScene("main", LoadSceneMode.Single);
		}
		if(Input.touchCount == 1){
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
				Ray touchRay = GenerateTouchRay (Input.GetTouch (0).position);
				RaycastHit hit;

				if (Physics.Raycast (touchRay.origin, touchRay.direction, out hit)) {
					
					if (hit.transform.gameObject.name != "Boundary") {
						tHit.text = hit.transform.gameObject.name;
						gObj = hit.transform.gameObject;
						if (gObj.GetComponent<Rigidbody> ()) {
							gObj_Rb = gObj.GetComponent<Rigidbody> (); 
							objPlane = new Plane (Camera.main.transform.forward *= 1, gObj_Rb.position);

							//Calcolo l'offset del touch
							Ray mRay = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
							float rayDistance;
							objPlane.Raycast (mRay, out rayDistance);
							m0 = gObj_Rb.position - mRay.GetPoint (rayDistance);

						}
					} else {
						tHit.text = hit.transform.gameObject.name;
						return;
					}
				}
			} else if (Input.GetTouch (0).phase == TouchPhase.Moved && gObj) {

				tHit.text = gObj.name;

				Ray mRay = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				float rayDistance;
				if (objPlane.Raycast (mRay, out rayDistance)) {
					gObj.GetComponent<Rigidbody> ().position = mRay.GetPoint (rayDistance) + m0;
				}
			} else if (Input.GetTouch (0).phase == TouchPhase.Ended &&  gObj) {
				gObj = null;
			}
		}
	}
}
